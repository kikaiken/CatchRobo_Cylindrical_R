/*
 * robotstate.c
 *
 *  Created on: 2018/08/12
 *      Author: Ryohei
 */

#include "robotstate.h"
#include "microswitch.h"
#include "motor.h"
#include "encoder.h"

//円筒座標系
struct_cyl poscyl;
struct_cyl refcyl;

//直交座標系
struct_rec posrec;
struct_rec refrec;

void ResetPos(void) {
	//原点復帰
	while(!MicroSwitchIsOn(MICSW_Z_MAX)){
		CoordinateSetDuty(MOTOR_BRAKE, MOTOR_BRAKE, 0.5f);
	}
	while(!MicroSwitchIsOn(MICSW_THETA_MIN)){
		CoordinateSetDuty(MOTOR_BRAKE, -0.25f, 0.2f);
	}
	while(!MicroSwitchIsOn(MICSW_R_MIN)){
		CoordinateSetDuty(-0.4f,-0.1f,0.2f);
	}
	CoordinateSetDuty(MOTOR_BRAKE, MOTOR_BRAKE, MOTOR_BRAKE);

	//現在円筒座標
	poscyl.r     = 0.13f;
	poscyl.theta = (float)(-10.0f * M_PI) / 180.0f;
	poscyl.z     = 0;

	//目標円筒座標
	refcyl.r     = 0.13f;
	refcyl.theta = 0;
	refcyl.z     = 0;

	//現在直交座標
	posrec.x = 0.13f;
	posrec.y = 0;
	posrec.z = 0;

	//目標直交座標
	refrec.x = 0.13f;
	refrec.y = 0;
	refrec.z = 0;
}
