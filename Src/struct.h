/*
 * struct.h
 *
 *  Created on: 2018/06/03
 *      Author: Ryohei
 */

#ifndef STRUCT_H_
#define STRUCT_H_

extern SPI_HandleTypeDef hspi3;

extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim4;
extern TIM_HandleTypeDef htim6;
extern TIM_HandleTypeDef htim7;
extern TIM_HandleTypeDef htim8;
extern TIM_HandleTypeDef htim11;

extern UART_HandleTypeDef huart2;

#endif /* STRUCT_H_ */
