/*
 * gyro.c
 *
 *  Created on: 2018/08/15
 *      Author: Ryohei
 */

#include "gyro.h"

//マクロ
#define DEG2RAD(x)		(float)((x) * M_PI / 180)
#define RAD2DEG(x)		(float)((x) * 180 / M_PI)

//ファイル内グローバル変数
volatile static float angvel_offset = 0;

//static関数プロトタイプ宣言
static int8_t GyroInitSub(void);
static void GyroSend(uint8_t, uint8_t);
static uint8_t GyroRead(uint8_t);
static inline void GyroSelect(void);
static inline void GyroDeselect(void);
static inline void GyroWait(void);

/*
 * ジャイロZ軸入力
 * @param
 * @return	角速度(rad/s)
 */
float GyroGetAngVel(void) {
	volatile uint8_t data_h, data_l;
	volatile int16_t data;

	//通信設定変更
	GYRO_SPI_HANDLER.Init.FirstBit = SPI_FIRSTBIT_MSB;
	HAL_SPI_Init(&GYRO_SPI_HANDLER);

	GyroSelect();		//CSオン

	data_h = GyroRead(GYRO_ZOUT_H);
	GyroWait();
	data_l = GyroRead(GYRO_ZOUT_L);
	data = (data_h << 8) + data_l;

	GyroDeselect();		//CSオフ

	if (data >= 0) {
		return (float)((float)(DEG2RAD(data * 0.06103516f) - angvel_offset) * 1.0f);
	} else {
		return (float)((float)(DEG2RAD(data * 0.06103516f) - angvel_offset) * 1.0f);
	}
}

/*
 * 角速度のゼロ点を測定
 * @param
 * @return
 */
void GyroGetAngVelOffset(void) {
	int32_t t = 0;
	volatile float angvel_sum = 0;
	const uint16_t num_of_sample = 300;
	angvel_offset = 0;

	for (int32_t i = 0; i < 3; i++) {
		SetLED(1);
		HAL_Delay(200);
		SetLED(0);
		HAL_Delay(200);
	}

	//num_of_sample回の測定の平均をとる
	for (int32_t i = 0; i < num_of_sample; i++) {
		angvel_sum = angvel_sum + GyroGetAngVel();
		HAL_Delay(1);
		if (t < 25) {
			SetLED(1);
		} else if (t < 50) {
			SetLED(0);
		} else {
			t = 0;
		}
		t++;
	}
	angvel_offset = angvel_sum / num_of_sample;

	//表示
	xprintf("Gyro Angular Velocity Offset = %dE-3\n", (int)(angvel_offset * 1000));
}

/*
 * 初期設定複数回呼び出し
 * @param
 * @return
 * @note	電源投入直後は不安定になる可能性があるため
 */
void GyroInit(void) {
	volatile int8_t check;
	volatile int8_t count = 0;
	HAL_Delay(10);
	do{
		check = GyroInitSub();
		GyroWait();
		count++;
		if (count >= 10) {
			xprintf("Gyro Communication Error\n");
			return;
		}
	} while (check != 1);
	xprintf("Gyro Communication Succeeded\n");
}

/*
 * 初期設定
 * @param
 * @return
 */
static int8_t GyroInitSub(void) {
	//通信設定変更
	GYRO_SPI_HANDLER.Init.FirstBit = SPI_FIRSTBIT_MSB;
	HAL_SPI_Init(&GYRO_SPI_HANDLER);

	//CSオン
	GyroSelect();

	//WHO_AM_I
	uint8_t gyro_check;
	gyro_check = GyroRead(MPU9255_WHO_AM_I);
	xprintf("Gyro Who am I = %d\n", gyro_check);

	//設定
	GyroSend(MPU9255_PWR_MGMT_1,  0x00);
	GyroWait();
	GyroSend(MPU9255_PWR_MGMT_2,  0b111110);		//ジャイロZ軸のみENABLE
	GyroWait();
	GyroSend(MPU9255_INT_PIN_CFG, 0x02);
	GyroWait();
	GyroSend(MPU9255_GYRO_CONFIG, 0x18);    		//Full Scale : 2000dps

	//CSオフ
	GyroDeselect();

	if (gyro_check == 0x71){
		return 1;
	} else {
		return 0;
	}
}

/*
 * データ書き込み
 * @param	address : MPU9250レジスタアドレス
 * 			data : 書き込むデータ
 * @return
 */
static void GyroSend(uint8_t address, uint8_t data) {
	uint8_t txdata[2] = {address, data};
	uint8_t rxdata[2] = {};
	GyroWait();
	HAL_SPI_TransmitReceive(&GYRO_SPI_HANDLER, &txdata[0], &rxdata[0], 1, HAL_MAX_DELAY);
	HAL_SPI_TransmitReceive(&GYRO_SPI_HANDLER, &txdata[1], &rxdata[1], 1, HAL_MAX_DELAY);
}

/*
 * データ読み込み
 * @param	address : MPU9250レジスタアドレス
 * @return	読み込んだデータ
 */
static uint8_t GyroRead(uint8_t address) {
	uint8_t txdata[2] = {(address | 0x80), 0x00};
	uint8_t rxdata[2] = {};
	GyroWait();
	HAL_SPI_TransmitReceive(&GYRO_SPI_HANDLER, &txdata[0], &rxdata[0], 1, HAL_MAX_DELAY);
	HAL_SPI_TransmitReceive(&GYRO_SPI_HANDLER, &txdata[1], &rxdata[1], 1, HAL_MAX_DELAY);
	return rxdata[1];
}

/*
 * チップセレクト
 * @param
 * @return
 */
static inline void GyroSelect(void) {
	HAL_GPIO_WritePin(GYRO_CS_PORT, GYRO_CS_PIN, GPIO_PIN_RESET);
}

/*
 * チップセレクト解除
 * @param
 * @return
 */
static inline void GyroDeselect(void) {
	HAL_GPIO_WritePin(GYRO_CS_PORT, GYRO_CS_PIN, GPIO_PIN_SET);
}

/*
 * チップセレクト後の待ち時間
 * @param
 * @return
 */
static inline void GyroWait(void) {
	for (volatile int8_t i = 0; i < 1; i++) ;
}
