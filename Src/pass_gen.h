#ifndef PASS_GEN_H
#define PASS_GEN_H

#include"robotstate.h"

struct pass_function {
  float co_slope_y; //傾き
  float co_slope_z; //傾き
  float co_const_y; //切片
  float co_const_z; //切片
  float diff_x;
  struct_rec start_pos;
  struct_rec end_pos;
};

void generate_function(struct_rec *start_pos,struct_rec *end_pos,struct pass_function pass_funcs[]);
void generate_points_csv(struct pass_function pass_funcs[]);
void draw_circle();
int generate_pass(struct_rec pos_array[]);
int generate_straight_pass(uint32_t current_index,struct_rec *start_pos,struct_rec *end_pos,struct_rec pos_array[]);

#endif
