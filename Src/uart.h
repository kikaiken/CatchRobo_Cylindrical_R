/*
 * uart.h
 */

#ifndef UART_H_
#define UART_H_


#include "stm32f4xx_hal.h"
#include "xprintf.h"
#include "struct.h"

//UARTの設定用構造体
#define H_UART		huart2

//プロトタイプ宣言
void XprintfInit(void);
void UARTPutChar(uint8_t);
uint8_t UARTGetChar(void);


#endif	//_UART_H_
