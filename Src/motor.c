/*
 * motor.c
 *
 *  Created on: 2018/06/03
 *      Author: Ryohei
 */

#include "motor.h"

/*-----------------------------------------------
 * 座標軸モータ
-----------------------------------------------*/
//static void CoordinateSetDuty(float, float, float);
static void CoordinateSetDirection(float, float, float);

/*
 * 指定された直行座標位置に動かす
 * @param
 * @return
 */
void CoordinateSetPosXYZ(float theta_angvel) {
	//座標変換
	struct_cyl cyl;
	struct_rec rec;
	rec = refrec;
	Rec2Cyl(&rec, &cyl);
	refcyl = cyl;

	//出力
	CoordinateSetPosRTZ(theta_angvel);
}

/*
 * 目標円筒座標位置に動かす
 * @param
 * @return
 */
void CoordinateSetPosRTZ(float theta_angvel) {
	float duty_r, duty_theta, duty_z;
	static float prev_z = 0;

	//各モータのDuty比を決定
	duty_r     = (poscyl.r     - refcyl.r)     * (-17.5f);
	duty_theta = (poscyl.theta - refcyl.theta) * (-7.5f) + theta_angvel * (-0.05f);
	duty_z     = (poscyl.z     - refcyl.z)     * (-90.0f) + (poscyl.z - prev_z) * (-130.0f);

	//前回値保存
	prev_z = poscyl.z;

	//モータに出力
	CoordinateSetDuty(duty_r, duty_theta, duty_z);
}

/*
 * 指定されたduty比でモータを回す
 * @param	spd_duty : duty比。-1 ~ 1
 * @return
 */
void CoordinateSetDuty(float duty_r, float duty_theta, float duty_z) {
	uint16_t pwm_r = 0, pwm_theta = 0, pwm_z = 0;

	//-1 ~ 1に制限
	if (duty_r     != MOTOR_BRAKE) {duty_r     = fmaxf(fminf(duty_r,     1), -1);}
	if (duty_theta != MOTOR_BRAKE) {duty_theta = fmaxf(fminf(duty_theta, 1), -1);}
	if (duty_z     != MOTOR_BRAKE) {duty_z     = fmaxf(fminf(duty_z,     1), -1);}

	//回転方向を設定
	CoordinateSetDirection(duty_r, duty_theta, duty_z);

	//Duty比の絶対値をとるとともに、DUTY比を制限する
	if (duty_r != MOTOR_BRAKE) {
		duty_r = fabsf(duty_r) * COORDINATE_DUTY_LIMIT;
		pwm_r  = duty_r * (float)(COORDINATE_TIM_FREQ / COORDINATE_PWM_FREQ);
	}

	if (duty_theta != MOTOR_BRAKE) {
		duty_theta = fabsf(duty_theta) * COORDINATE_DUTY_LIMIT;
		pwm_theta  = duty_theta * (float)(COORDINATE_TIM_FREQ / COORDINATE_PWM_FREQ);
	}

	if (duty_z != MOTOR_BRAKE) {
		duty_z = fabsf(duty_z) * COORDINATE_DUTY_LIMIT;
		pwm_z  = duty_z * (float)(COORDINATE_TIM_FREQ / COORDINATE_PWM_FREQ);
	}

	//各チャンネルにコンペアマッチ値を設定
	__HAL_TIM_SetCompare(&COORDINATE_TIM_HANDLER, PWM_R_TIM_CH, 	pwm_r);
	__HAL_TIM_SetCompare(&COORDINATE_TIM_HANDLER, PWM_THETA_TIM_CH, pwm_theta);
	__HAL_TIM_SetCompare(&COORDINATE_TIM_HANDLER, PWM_Z_TIM_CH, 	pwm_z);
}

/*
 * dutyの正負からモータの回転方向を指定
 * @param	duty_r, duty_theta, duty_z, duty_hand : 各軸のduty比
 * @return
 */
static void CoordinateSetDirection(float duty_r, float duty_theta, float duty_z) {
	if (duty_r >= 0) {
		HAL_GPIO_WritePin(MOTOR_R_1_PORT, MOTOR_R_1_PIN, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(MOTOR_R_2_PORT, MOTOR_R_2_PIN, GPIO_PIN_SET);
	} else if (duty_r < 0){
		HAL_GPIO_WritePin(MOTOR_R_1_PORT, MOTOR_R_1_PIN, GPIO_PIN_SET);
		HAL_GPIO_WritePin(MOTOR_R_2_PORT, MOTOR_R_2_PIN, GPIO_PIN_RESET);
	} else if (duty_r == MOTOR_BRAKE) {
		HAL_GPIO_WritePin(MOTOR_R_1_PORT, MOTOR_R_1_PIN, GPIO_PIN_SET);
		HAL_GPIO_WritePin(MOTOR_R_2_PORT, MOTOR_R_2_PIN, GPIO_PIN_SET);
	} else {
		HAL_GPIO_WritePin(MOTOR_R_1_PORT, MOTOR_R_1_PIN, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(MOTOR_R_2_PORT, MOTOR_R_2_PIN, GPIO_PIN_RESET);
	}

	if (duty_theta >= 0) {
		HAL_GPIO_WritePin(MOTOR_THETA_1_PORT, MOTOR_THETA_1_PIN, GPIO_PIN_SET);
		HAL_GPIO_WritePin(MOTOR_THETA_2_PORT, MOTOR_THETA_2_PIN, GPIO_PIN_RESET);
	} else if (duty_theta < 0){
		HAL_GPIO_WritePin(MOTOR_THETA_1_PORT, MOTOR_THETA_1_PIN, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(MOTOR_THETA_2_PORT, MOTOR_THETA_2_PIN, GPIO_PIN_SET);
	} else if (duty_theta == MOTOR_BRAKE) {
		HAL_GPIO_WritePin(MOTOR_THETA_1_PORT, MOTOR_THETA_1_PIN, GPIO_PIN_SET);
		HAL_GPIO_WritePin(MOTOR_THETA_2_PORT, MOTOR_THETA_2_PIN, GPIO_PIN_SET);
	} else {
		HAL_GPIO_WritePin(MOTOR_THETA_1_PORT, MOTOR_THETA_1_PIN, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(MOTOR_THETA_2_PORT, MOTOR_THETA_2_PIN, GPIO_PIN_RESET);
	}

	if (duty_z >= 0) {
		HAL_GPIO_WritePin(MOTOR_Z_1_PORT, MOTOR_Z_1_PIN, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(MOTOR_Z_2_PORT, MOTOR_Z_2_PIN, GPIO_PIN_SET);
	} else if (duty_z < 0){
		HAL_GPIO_WritePin(MOTOR_Z_1_PORT, MOTOR_Z_1_PIN, GPIO_PIN_SET);
		HAL_GPIO_WritePin(MOTOR_Z_2_PORT, MOTOR_Z_2_PIN, GPIO_PIN_RESET);
	} else if (duty_z == MOTOR_BRAKE) {
		HAL_GPIO_WritePin(MOTOR_Z_1_PORT, MOTOR_Z_1_PIN, GPIO_PIN_SET);
		HAL_GPIO_WritePin(MOTOR_Z_2_PORT, MOTOR_Z_2_PIN, GPIO_PIN_SET);
	} else {
		HAL_GPIO_WritePin(MOTOR_Z_1_PORT, MOTOR_Z_1_PIN, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(MOTOR_Z_2_PORT, MOTOR_Z_2_PIN, GPIO_PIN_RESET);
	}
}

/*
 * タイマENABLE
 * @param
 * @return
 */
void CoordinateEnable(void) {
	CoordinateSetDuty(0, 0, 0);
	HAL_TIM_PWM_Start(&COORDINATE_TIM_HANDLER, PWM_R_TIM_CH);
	HAL_TIM_PWM_Start(&COORDINATE_TIM_HANDLER, PWM_THETA_TIM_CH);
	HAL_TIM_PWM_Start(&COORDINATE_TIM_HANDLER, PWM_Z_TIM_CH);
}

/*
 * タイマDISABLE
 * @param
 * @return
 */
void CoordinateDisable(void) {
	CoordinateSetDuty(0, 0, 0);
	HAL_TIM_PWM_Stop(&COORDINATE_TIM_HANDLER, PWM_R_TIM_CH);
	HAL_TIM_PWM_Stop(&COORDINATE_TIM_HANDLER, PWM_THETA_TIM_CH);
	HAL_TIM_PWM_Stop(&COORDINATE_TIM_HANDLER, PWM_Z_TIM_CH);
}


/*-----------------------------------------------
 * 手先サーボ
-----------------------------------------------*/
/*
 * ハンド角度指定
 * @param	yaw : yaw軸角度(-180 ~ 180)
 * 			pitch : pitch軸角度(-90 ~ 90)
 * 			remove : removeモータ角度(-90 ~ 90)
 * @return
 */
void HandSetAngle(float yaw, float pitch) {
	//角度範囲を制限
	yaw    = fmaxf(fminf(yaw, 180), -180) / HAND_YAW_GEAR_RATIO;
	pitch  = fmaxf(fminf(pitch, 90), -90);

	//パルス幅(ms)を決める
	yaw    = HAND_YAW_CENTER_PULSE    + (float)(HAND_YAW_90DEG_PULSE    * yaw)    / 90;
	pitch  = HAND_PITCH_CENTER_PULSE  + (float)(HAND_PITCH_90DEG_PULSE  * pitch)  / 90;

	//コンペアマッチ値を決める
	yaw    *= (float)(HAND_TIM_PERIOD / HAND_SIG_CYCLE);
	pitch  *= (float)(HAND_TIM_PERIOD / HAND_SIG_CYCLE);

	//パルス幅設定
	__HAL_TIM_SetCompare(&HAND_TIM_HANDLER, HAND_YAW_TIM_CH,    (int)yaw);
	__HAL_TIM_SetCompare(&HAND_TIM_HANDLER, HAND_PITCH_TIM_CH,  (int)pitch);
}

/*
 * タイマENABLE
 * @param
 * @return
 */
void HandEnable(void) {
	HandSetAngle(HAND_YAW_ZERO, HAND_PITCH_DOWN);
	HAL_TIM_PWM_Start(&HAND_TIM_HANDLER, HAND_YAW_TIM_CH);
	HAL_TIM_PWM_Start(&HAND_TIM_HANDLER, HAND_PITCH_TIM_CH);
}

/*
 * タイマDISABLE
 * @param
 * @return
 */
void HandDisable(void) {
	HAL_TIM_PWM_Stop(&HAND_TIM_HANDLER, HAND_YAW_TIM_CH);
	HAL_TIM_PWM_Stop(&HAND_TIM_HANDLER, HAND_PITCH_TIM_CH);
}

/*-----------------------------------------------
 * グリッパー
-----------------------------------------------*/
/*
 * グリッパー角度指定
 * @param	left, middle, right : 各グリッパーの角度
 * @return
 */
void GripperSetAngle(float left, float right) {
	//角度範囲を制限
	left    = fmaxf(fminf(left,  90), -90);
	right	= fmaxf(fminf(right, 90), -90);

	//パルス幅(ms)を決める
	left   = GRIPPER_L_CENTER_PULSE + (float)(GRIPPER_L_90DEG_PULSE * left)   / 90;
	right  = GRIPPER_R_CENTER_PULSE + (float)(GRIPPER_R_90DEG_PULSE * right)  / 90;

	//コンペアマッチ値を決める
	left   *= (float)(GRIPPER_TIM_PERIOD / GRIPPER_SIG_CYCLE);
	right  *= (float)(GRIPPER_TIM_PERIOD / GRIPPER_SIG_CYCLE);

	//パルス幅設定
	__HAL_TIM_SetCompare(&GRIPPER_L_TIM_HANDLER, GRIPPER_L_TIM_CH, (int)left);
	__HAL_TIM_SetCompare(&GRIPPER_R_TIM_HANDLER, GRIPPER_R_TIM_CH, (int)right);
}

/*
 * タイマENABLE
 * @param
 * @return
 */
void GripperEnable(void) {
	HAL_TIM_PWM_Start(&GRIPPER_L_TIM_HANDLER, GRIPPER_L_TIM_CH);
	HAL_TIM_PWM_Start(&GRIPPER_R_TIM_HANDLER, GRIPPER_R_TIM_CH);
	GripperSetAngle(GRIPPER_L_CLOSE, GRIPPER_R_CLOSE);
}

/*
 * タイマDISABLE
 * @param
 * @return
 */
void GripperDisable(void) {
	HAL_TIM_PWM_Stop(&GRIPPER_L_TIM_HANDLER, GRIPPER_L_TIM_CH);
	HAL_TIM_PWM_Stop(&GRIPPER_R_TIM_HANDLER, GRIPPER_R_TIM_CH);
}
