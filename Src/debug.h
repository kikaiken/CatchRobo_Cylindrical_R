/*
 * debug.h
 *
 *  Created on: 2018/08/22
 *      Author: nimda
 */

#ifndef DEBUG_H_
#define DEBUG_H_

#include "stm32f4xx_hal.h"
#include "xprintf.h"
#include "encoder.h"
#include "robotstate.h"
#include <math.h>

#define LED_PORT		GPIOC
#define LED_PIN			GPIO_PIN_3

//プロトタイプ宣言
void SetLED(int8_t);
void PosCylDebug();

#endif /* DEBUG_H_ */
