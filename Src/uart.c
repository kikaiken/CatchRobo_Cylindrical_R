/*
 * uart.c
 */

#include "uart.h"

/*
 * xprintf設定用関数
 * @param
 * @return
 */
void XprintfInit(void) {
	xdev_out(UARTPutChar);
	xdev_in(UARTGetChar);
}

/*
 * 一文字送信
 * @param	c : 送信する文字
 * @return
 */
void UARTPutChar(uint8_t c) {
	char buf[1];
	buf[0] = c;
	HAL_UART_Transmit(&H_UART, (uint8_t *)buf, sizeof(buf), 0xFFFF);
}

/*
 * 一文字受信
 * @param
 * @return	受信した文字
 */
uint8_t UARTGetChar(void) {
	uint8_t c = 0;
	char buf[1];
	HAL_UART_Receive(&H_UART, (uint8_t *)buf, sizeof(buf), 0xFFFF);
	c = buf[0];
	return c;
}
