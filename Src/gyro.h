/*
 * gyro.h
 *
 *  Created on: 2018/08/15
 *      Author: Ryohei
 */

#ifndef GYRO_H_
#define GYRO_H_


#include "stm32f4xx_hal.h"
#include "struct.h"
#include "xprintf.h"
#include "debug.h"
#include <math.h>

//SPI通信ハンドラ
#define GYRO_SPI_HANDLER		hspi3

//CSポート、ピン
#define GYRO_CS_PORT			GPIOD
#define GYRO_CS_PIN				GPIO_PIN_2

//レジスタ
#define MPU9255_WHO_AM_I        0x75
#define MPU9255_SMPLRT_DIV      0x19
#define MPU9255_CONFIG          0x1A
#define MPU9255_GYRO_CONFIG     0x1B
#define MPU9255_ACCEL_CONFIG    0x1C
#define MPU9255_ACCEL_CONFIG_2  0x1D
#define MPU9255_INT_PIN_CFG     0x37
#define ACCEL_XOUT_H            0x3B
#define ACCEL_XOUT_L            0x3C
#define ACCEL_YOUT_H            0x3D
#define ACCEL_YOUT_L            0x3E
#define ACCEL_ZOUT_H            0x3F
#define ACCEL_ZOUT_L            0x40
#define GYRO_XOUT_H             0x43
#define GYRO_XOUT_L             0x44
#define GYRO_YOUT_H             0x45
#define GYRO_YOUT_L             0x46
#define GYRO_ZOUT_H             0x47
#define GYRO_ZOUT_L             0x48
#define MPU9255_PWR_MGMT_1      0x6B
#define MPU9255_PWR_MGMT_2      0x6C

//プロトタイプ宣言
float GyroGetAngVel(void);
void GyroGetAngVelOffset(void);
void GyroInit(void);


#endif /* GYRO_H_ */
