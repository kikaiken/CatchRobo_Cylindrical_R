#include <stdio.h>
#include <math.h>
#include "xprintf.h"
#include "pass_gen.h"


#define NO_ENTRY_AREA_R (float)0.3
#define NO_ENTRY_AREA_X (float)-0.047
#define NO_ENTRY_AREA_Z (float)-0.307

#define FIELD_POS_X       265.0f/1000
#define FIELD_POS_Y       300.0f/1000
#define FIELD_POS_Z       -400.0f/1000

#define SHOOTING_POS_X    -153.0f/1000
#define SHOOTING_POS_Y    319.0f/1000
#define SHOOTING_POS_Z    -5.0f/1000

#define INFINITE 10000
#define DIFF_N 5000
#define AUTO_SPEED 0.0027f
/*
 * pass_funcs[0] : ビンゴエリアから垂直に上昇する
 * pass_funcs[1] : 進入禁止エリアの接線まで水平に移動する
 * pass_funcs[2] : 進入禁止エリアの接線で、フィールドまで移動する
 */

int generate_pass(struct_rec pos_array[]){

  static struct_rec field_pos = {FIELD_POS_X,FIELD_POS_Y,FIELD_POS_Z};

  struct pass_function pass_funcs[3];
  float pos=0;
  int index=0;
  struct_rec end_pos;
  struct_rec tmp = {0,0,0};

  if(refrec.x > NO_ENTRY_AREA_X){
	  //field_pos = refrec;
	  //field_pos.z = fminf(0.0f,refrec.z+0.10f);
	  end_pos.x = SHOOTING_POS_X;
	  end_pos.y = SHOOTING_POS_Y;
	  end_pos.z = SHOOTING_POS_Z;
	  if(refrec.x < NO_ENTRY_AREA_X + NO_ENTRY_AREA_R){
	   struct_rec via_pos = {FIELD_POS_X,FIELD_POS_Y,refrec.z};
      index = generate_straight_pass(0,&refrec,&via_pos,pos_array);
      generate_function(&end_pos,&via_pos,pass_funcs);
	  }else{
      generate_function(&end_pos,&refrec,pass_funcs);
    }
    for(int i=2;i>0;i--){
      pos=pass_funcs[i].end_pos.x;
      do{
        tmp.x = pos;
        tmp.y = pass_funcs[i].co_slope_y*pos + pass_funcs[i].co_const_y;
        tmp.z = pass_funcs[i].co_slope_z*pos + pass_funcs[i].co_const_z;
        pos_array[index] = tmp;
        index++;
        pos-= AUTO_SPEED * pass_funcs[i].diff_x;
      }while(tmp.x >= pass_funcs[i].start_pos.x);
    }
    pos = pass_funcs[0].end_pos.z;
    if(pass_funcs[0].end_pos.z > pass_funcs[0].start_pos.z){
      while(pass_funcs[0].start_pos.z < pos){
        tmp.x = pass_funcs[0].start_pos.x;
        tmp.y = pass_funcs[0].end_pos.y;
        tmp.z = pos;
        pos_array[index] = tmp;
        index++;
        pos-= AUTO_SPEED * pass_funcs[0].diff_x;
      }
    }else{
      while(pass_funcs[0].start_pos.z > pos){
        tmp.x = pass_funcs[0].start_pos.x;
        tmp.y = pass_funcs[0].end_pos.y;
        tmp.z = pos;
        pos_array[index] = tmp;
        index++;
        pos+= AUTO_SPEED * pass_funcs[0].diff_x;
      }
    }
  }else{
	  if(field_pos.x < NO_ENTRY_AREA_X + NO_ENTRY_AREA_R){
		  end_pos.x = FIELD_POS_X;
		  end_pos.y = FIELD_POS_Y;
		  end_pos.z = field_pos.z;
	  }else{
		  end_pos = field_pos;
	  }
    generate_function(&refrec,&end_pos,pass_funcs);
    pos = pass_funcs[0].start_pos.z;
    if(pass_funcs[0].start_pos.z < pass_funcs[0].end_pos.z){
      while(pass_funcs[0].end_pos.z > pos){
        tmp.x = pass_funcs[0].start_pos.x;
        tmp.y = pass_funcs[0].end_pos.y;
        tmp.z = pos;
        pos_array[index] = tmp;
        index++;
        pos+= AUTO_SPEED *pass_funcs[0].diff_x;
      }
    }else{
      while(pass_funcs[0].end_pos.z < pos){
        tmp.x = pass_funcs[0].start_pos.x;
        tmp.y = pass_funcs[0].end_pos.y;
        tmp.z = pos;
        pos_array[index] = tmp;
        index++;
        pos-= AUTO_SPEED * pass_funcs[0].diff_x;
      }
    }
    pos=pass_funcs[1].start_pos.x;
    //printf("%f　%f\n",(pass_funcs[1].co_slope_z*1000),(pass_funcs[1].co_const_z*1000));
    for(int i=1;i<3;i++){
      do{
        tmp.x = pos;
        tmp.y = pass_funcs[i].co_slope_y*pos + pass_funcs[i].co_const_y;
        tmp.z = pass_funcs[i].co_slope_z*pos + pass_funcs[i].co_const_z;
        pos_array[index] = tmp;
        index++;
        pos+= AUTO_SPEED *pass_funcs[i].diff_x;
      }while(tmp.x <= pass_funcs[i].end_pos.x);
    }
    if(field_pos.x < NO_ENTRY_AREA_X + NO_ENTRY_AREA_R){
    	struct_rec via_pos = {FIELD_POS_X,FIELD_POS_Y,field_pos.z};
    	index = generate_straight_pass(index,&via_pos,&field_pos,pos_array);
    }
  }
  return index;
}

void generate_function(struct_rec *start_pos, struct_rec *end_pos,struct pass_function pass_funcs[])
{
  //y(x)に関する計算
  float xy_func_slope = (start_pos->y-end_pos->y)/(start_pos->x-end_pos->x);
  /*xprintf("%d %d %d %d %d\n",(int)((start_pos->y-end_pos->y)*1000),(int)((start_pos->x-end_pos->x)*1000),
		  (int)(xy_func_slope*1000),(int)(start_pos->x*1000),(int)(end_pos->x*1000));*/
  float xy_func_const = start_pos->y-xy_func_slope*start_pos->x;
  //y(x)の関数を代入
  pass_funcs[0].co_slope_y = 0;
  pass_funcs[0].co_const_y = 0;
  pass_funcs[1].co_slope_y = xy_func_slope;
  pass_funcs[1].co_const_y = xy_func_const;
  pass_funcs[2].co_slope_y = xy_func_slope;
  pass_funcs[2].co_const_y = xy_func_const;

  //進入禁止エリアの円筒の中心を原点として接する直線を計算する
  float tmp_x = end_pos->x - NO_ENTRY_AREA_X;
  float tmp_z = end_pos->z - NO_ENTRY_AREA_Z;
  float tmp_r = NO_ENTRY_AREA_R;
  float A =  sqrtf(tmp_x*tmp_x + tmp_z*tmp_z - tmp_r*tmp_r);
  float co_slope= -1*((tmp_r*tmp_x - tmp_z*A)/(tmp_r*tmp_z + tmp_x*A));

  
  //座標をずらしながら関数を代入
  float tmp_1 = co_slope;
  float tmp_2 = end_pos->z - tmp_1*end_pos->x;
  pass_funcs[2].co_slope_z = tmp_1;
  pass_funcs[2].co_const_z = tmp_2;

  //z(x)の関数を代入
  pass_funcs[0].co_slope_z = INFINITE;
  pass_funcs[0].co_const_z = start_pos->z;
  pass_funcs[0].diff_x = 1;
  pass_funcs[1].co_slope_z = 0;
  pass_funcs[1].co_const_z = NO_ENTRY_AREA_Z + NO_ENTRY_AREA_R;

  //各関数の範囲を決定する
  pass_funcs[0].start_pos = *start_pos;
  pass_funcs[0].end_pos = *start_pos;
  pass_funcs[0].end_pos.z = NO_ENTRY_AREA_Z + NO_ENTRY_AREA_R;

  tmp_1 = -(pass_funcs[2].co_const_z - pass_funcs[1].co_const_z)/(pass_funcs[2].co_slope_z - pass_funcs[1].co_slope_z);
  tmp_2 = pass_funcs[1].co_slope_y*tmp_1 + pass_funcs[1].co_const_y;
  pass_funcs[1].start_pos = pass_funcs[0].end_pos;
  pass_funcs[1].end_pos.x = tmp_1;
  pass_funcs[1].end_pos.y = tmp_2;
  pass_funcs[1].end_pos.z = NO_ENTRY_AREA_Z + NO_ENTRY_AREA_R;

  pass_funcs[2].start_pos = pass_funcs[1].end_pos;
  pass_funcs[2].end_pos = *end_pos;

  //直線の距離が1となるdiff_xを求める
  tmp_1 = pow(pass_funcs[1].co_slope_y,2) + 1;
  pass_funcs[1].diff_x = 1/sqrt(tmp_1);
  tmp_1 = pow(pass_funcs[2].co_slope_y,2) + pow(pass_funcs[2].co_slope_z,2) + 1;
  pass_funcs[2].diff_x = 1/sqrt(tmp_1);
  /*
  for(int i=0;i<3;i++){
    printf("Func:%d Start x:%f y:%f z:%f  End x:%f y:%f z:%f\n",i,
        pass_funcs[i].start_pos.x,
        pass_funcs[i].start_pos.y,
        pass_funcs[i].start_pos.z,
        pass_funcs[i].end_pos.x,
        pass_funcs[i].end_pos.y,
        pass_funcs[i].end_pos.z);
    printf(" co_slope_y:%f co_const_y:%f co_slope_z:%f co_const_z:%f\n",
        pass_funcs[i].co_slope_y,
        pass_funcs[i].co_const_y,
        pass_funcs[i].co_slope_z,
        pass_funcs[i].co_const_z);
  }
  */
}

int generate_straight_pass(uint32_t current_index,struct_rec *start_pos,struct_rec *end_pos,struct_rec pos_array[]){
  float co_slope = (start_pos->y - end_pos->y)/(start_pos->x - end_pos->x);
  float co_const = -co_slope*start_pos->x + start_pos->y;
  float tmp_1 = pow(co_slope,2) + 1;
  float diff_x = 1/sqrt(tmp_1);
  uint32_t index = current_index;
  struct_rec tmp;
  float pos = start_pos->x;
  if(start_pos->x > end_pos->x){
    while(pos >= end_pos->x){
      tmp.x = pos;
      tmp.y = co_slope*pos + co_const;
      tmp.z = start_pos->z;
      pos_array[index] = tmp;
      index++;
      pos-=AUTO_SPEED*diff_x;
    }
  }else{
    while(pos <= end_pos->x){
      tmp.x = pos;
      tmp.y = co_slope*pos + co_const;
      tmp.z = start_pos->z;
      pos_array[index] = tmp;
      index++;
      pos+=AUTO_SPEED*diff_x;
    }
  }
  return index;
}
