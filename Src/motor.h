/*
 * motor.h
 *
 *  Created on: 2018/06/03
 *      Author: Ryohei
 */

#ifndef MOTOR_H_
#define MOTOR_H_


#include "math.h"
#include "stm32f4xx_hal.h"
#include "struct.h"
#include "encoder.h"
#include "autoctrl.h"

/*-----------------------------------------------
 * 座標軸DCモータ(r, theta, z)
-----------------------------------------------*/
void CoordinateSetPosXYZ(float);
void CoordinateSetPosRTZ(float);
void CoordinateSetDuty(float duty_r, float duty_theta, float duty_z);
void CoordinateEnable(void);
void CoordinateDisable(void);

//座標軸DCモータタイマハンドラ
#define COORDINATE_TIM_HANDLER		htim1

//PWM周波数
#define COORDINATE_PWM_FREQ			10000

//タイマ周波数
#define COORDINATE_TIM_FREQ			84000000

//Duty比のリミット
#define COORDINATE_DUTY_LIMIT		0.55f

//モータPWMチャンネル
#define PWM_R_TIM_CH				TIM_CHANNEL_3
#define PWM_THETA_TIM_CH			TIM_CHANNEL_1
#define PWM_Z_TIM_CH				TIM_CHANNEL_2

//DCモータ回転方向GPIO
//R軸
#define MOTOR_R_1_PORT				GPIOC
#define MOTOR_R_1_PIN				GPIO_PIN_5
#define MOTOR_R_2_PORT				GPIOB
#define MOTOR_R_2_PIN				GPIO_PIN_0

//Theta軸
#define MOTOR_THETA_1_PORT			GPIOB
#define MOTOR_THETA_1_PIN			GPIO_PIN_10
#define MOTOR_THETA_2_PORT			GPIOB
#define MOTOR_THETA_2_PIN			GPIO_PIN_12

//Z軸
#define MOTOR_Z_1_PORT				GPIOB
#define MOTOR_Z_1_PIN				GPIO_PIN_1
#define MOTOR_Z_2_PORT				GPIOB
#define MOTOR_Z_2_PIN				GPIO_PIN_2

//モータブレーキ
#define MOTOR_BRAKE					1000000


/*-----------------------------------------------
 * 手先サーボ
-----------------------------------------------*/
void HandSetAngle(float, float);
void HandEnable(void);
void HandDisable(void);

//ヨー軸角度
#define HAND_YAW_ZERO				0

//ピッチ軸角度
#define HAND_PITCH_UP				-75
#define HAND_PITCH_DOWN				14

//手先サーボタイマハンドラ
#define HAND_TIM_HANDLER			htim8

//タイマの周期のコンペアマッチ値
#define HAND_TIM_PERIOD				16800

//サーボの信号周期(ms)
#define HAND_SIG_CYCLE				20

//サーボ信号チャンネル
#define HAND_YAW_TIM_CH				TIM_CHANNEL_3
#define HAND_PITCH_TIM_CH			TIM_CHANNEL_4

//Yaw軸ギヤ比
#define HAND_YAW_GEAR_RATIO			(float)(32 / 15)

//中央パルス幅(ms)
#define HAND_YAW_CENTER_PULSE		1.7
#define HAND_PITCH_CENTER_PULSE		1.5

//90度パルス幅(ms)
#define HAND_YAW_90DEG_PULSE		1.0
#define HAND_PITCH_90DEG_PULSE		0.99


/*-----------------------------------------------
 * グリッパー
-----------------------------------------------*/
//プロトタイプ宣言
void GripperSetAngle(float, float);
void GripperEnable(void);
void GripperDisable(void);

//ハンド開閉
#define GRIPPER_L_OPEN				-37
#define GRIPPER_L_CLOSE				20

#define GRIPPER_R_OPEN				72
#define GRIPPER_R_CLOSE				23

//手先サーボタイマハンドラ
#define GRIPPER_L_TIM_HANDLER		htim8
#define GRIPPER_R_TIM_HANDLER		htim11

//タイマの周期のコンペアマッチ値
#define GRIPPER_TIM_PERIOD			16800

//サーボの信号周期(ms)
#define GRIPPER_SIG_CYCLE			20

//サーボ信号チャンネル
#define GRIPPER_L_TIM_CH			TIM_CHANNEL_1
#define GRIPPER_R_TIM_CH			TIM_CHANNEL_1

//中央パルス幅(ms)
#define GRIPPER_L_CENTER_PULSE		1.5f
#define GRIPPER_R_CENTER_PULSE		1.5f

//90度パルス幅(ms)
#define GRIPPER_L_90DEG_PULSE		1.0f
#define GRIPPER_R_90DEG_PULSE		1.0f


#endif /* MOTOR_H_ */
