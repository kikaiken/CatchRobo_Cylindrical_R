/*
 * debug.c
 *
 *  Created on: 2018/08/22
 *      Author: nimda
 */

#include "debug.h"

#define RAD2DEG(x)		(float)((x) * 180 / M_PI)

void SetLED(int8_t x) {
	HAL_GPIO_WritePin(LED_PORT, LED_PIN, ((x & 0b00000001) ? (GPIO_PIN_SET) : (GPIO_PIN_RESET)));
}

void PosCylDebug(){
	  GetPosCyl();
	  xprintf("%d %d %d\n", (int)(poscyl.r * 1000), (int)RAD2DEG(poscyl.theta), (int)(poscyl.z * 1000));
	  HAL_Delay(500);
}
